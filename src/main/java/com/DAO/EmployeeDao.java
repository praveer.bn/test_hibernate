package com.DAO;

import com.bean.EmployeeDetails;

import java.util.List;

public interface EmployeeDao {
   void saveEmployee (EmployeeDetails employee);

     List<EmployeeDetails> showAllEmployees();

     void updateEmployee (int id, String ename, String enumber);

     void deleteEmployee (int id);
}
