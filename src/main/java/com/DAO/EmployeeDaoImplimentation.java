package com.DAO;

import com.bean.EmployeeDetails;
import com.connection.FactoryProvider;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImplimentation implements EmployeeDao{
    public void saveEmployee(EmployeeDetails employee) {
        Session session= FactoryProvider.getFactory().openSession();
        Transaction transaction=session.beginTransaction();
        session.save(employee);
        transaction.commit();
        session.close();
    }

    public List<EmployeeDetails> showAllEmployees() {
        List<EmployeeDetails> employeeList=new ArrayList();
        Session session=FactoryProvider.getFactory().openSession();
        Query query=session.createQuery("from EmployeeDetails");
        employeeList=query.list();
        return employeeList;
    }

    public void updateEmployee(int id, String ename, String enumber) {
        Session session= FactoryProvider.getFactory().openSession();
        Transaction transaction=session.beginTransaction();
        EmployeeDetails empdetails = (EmployeeDetails)session.load(EmployeeDetails.class, id);
        empdetails.setEname(ename);
        empdetails.setEnumber(enumber);
        session.update(empdetails);
        transaction.commit();
        session.close();
    }

    public void deleteEmployee(int id) {
        Session session= FactoryProvider.getFactory().openSession();
        Transaction transaction=session.beginTransaction();
        EmployeeDetails employeeDetails=session.get(EmployeeDetails.class,id);
        session.delete(employeeDetails);
        transaction.commit();
        session.close();

    }
}
