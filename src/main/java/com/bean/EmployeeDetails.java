package com.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
@Entity
public class EmployeeDetails implements Serializable {
    @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String ename;
    String enumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getEnumber() {
        return enumber;
    }

    public void setEnumber(String enumber) {
        this.enumber = enumber;
    }

    public EmployeeDetails(int id, String ename, String enumber) {
        this.id = id;
        this.ename = ename;
        this.enumber = enumber;
    }

    public EmployeeDetails() {
    }

    @Override
    public String toString() {
        return "EmployeeDetails{" +
                "id=" + id +
                ", ename='" + ename + '\'' +
                ", enumber='" + enumber + '\'' +
                '}';
    }
}
