package com.controller;

import com.DAO.EmployeeDao;
import com.DAO.EmployeeDaoImplimentation;
import com.bean.EmployeeDetails;

import javax.jws.WebService;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/EmployeeController")
public class EmployeeController extends HttpServlet {
    EmployeeDetails employeeDetails = new EmployeeDetails();
    EmployeeDaoImplimentation employeeDaoImplimentation = new EmployeeDaoImplimentation();
    EmployeeDao employeeDao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("addEmployee") != null) {
            String name = req.getParameter("ename");
            String number = req.getParameter("enumber");

            employeeDetails.setEname(name);
            employeeDetails.setEnumber(number);
            employeeDaoImplimentation.saveEmployee(employeeDetails);
            RequestDispatcher rd = req.getRequestDispatcher("EmployeeAdd.jsp");
            rd.forward(req, resp);

        }
    }
}





