package com.controller;

import com.DAO.EmployeeDaoImplimentation;
import com.bean.EmployeeDetails;
import com.connection.FactoryProvider;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
@WebServlet("/Showall")
public class Showall extends HttpServlet {
    @Override

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        List<EmployeeDetails> employeeList = new EmployeeDaoImplimentation().showAllEmployees();

        out.println("USER DATA : ");

        out.println("<table border=4 px >" +
                "  <tr>" +
                "    <th>Id</th><th>Employee Name</th><th>Employee Number </th>\n" +
                "  </tr>");


        for (int i = 0 ; i < employeeList.size();i++)
        {
            EmployeeDetails employee = employeeList.get(i);
            out.println("<tr><td>" + employee.getId() + "</td>" +
                    "<td>" + employee.getEname()+ "</td>" +
                    "<td>" + employee.getEnumber()+ "</td>" +
                    "</tr>"
            );

        }
        out.println("</table>");
        out.println("<a href='EmployeeAdd.jsp'><input type='button' value ='Back To Home'></a>");
        out.println("<a href='Update.jsp'><input type='button' value ='Update'></a>");
        out.println("<a href='Delete.jsp'><input type='button' value ='delete'></a>");
    }
    }

